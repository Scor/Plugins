<?php
namespace Home\Controller;
use Think\Controller;
class OrderController extends Controller {
    public function index(){
        if(IS_POST){
            if(I('openid')){
                $where['openid'] = I('openid');
                $User = new \Org\Util\AVQuery("_User");
                $Order = new \Org\Util\AVQuery("Order");
                $useravq = $User->getFind($where);
                $uid = $useravq->objectId;
                $ordermap= '{"uid":"'.$uid.'"}';
                $orderavq = $Order->getList1($ordermap,null,null,'-createdAt');
                for($i=0;$i<count($orderavq);$i++){
//                    dump($orderavq[$i]);
                    $list[$i]['orderid'] = $orderavq[$i]->oid;
                    $list[$i]['type'] = $orderavq[$i]->type;
                    $list[$i]['total'] = $orderavq[$i]->total;
                    $list[$i]['status'] = $orderavq[$i]->status;
                    $list[$i]['createdAt'] = $orderavq[$i]->createdAt;
//                    dump(date("Y/m/d H:i:00",strtotime($list[$i]['createdAt'])));
                }
                int_to_string_type($list);
                int_to_string_order($list);
//                dump($list);
                $this->assign("_list",$list);
            }else{

            }

        }

        $this->display();
    }
}