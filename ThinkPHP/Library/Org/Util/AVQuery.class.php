<?php
// +----------------------------------------------------------------------
// | ADM Software [ 我宁可 为做过的事而后悔，也不想 因为没尝试而后悔。 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------
namespace Org\Util;

class AVQuery {

    private $_classname;
    private $_dataArray = array();
    private $rest;
    public $objectId;
    

    function __construct($classname = '') { //初始化对象，将初始化值放在括号内
        $this->_classname = $classname;
        $this->rest = new Rest();
    }
    
    function get($objectId=""){
        $this->objectId=$objectId;
        $result= $this->rest->get($this->_classname, $this->objectId);
        $body_rwa=(array)json_decode($result->raw_body);
        $avobj=new AVObject($this->_classname);
        return $avobj->_QuerySet($body_rwa);
    }
    
    function getFind($where=""){        
        $result= $this->rest->getFind($this->_classname, $where);
        $body_rwa=(array)json_decode($result->raw_body);
        $avobj=new AVObject($this->_classname);
        $body_rwa_array=(array)$body_rwa['results'][0];
        return $avobj->_QuerySet($body_rwa_array);
    }
    
    function getList($where="",$limit=null,$skip=null){
        $result= $this->rest->getList($this->_classname,$where,$limit,$skip);
        return $result->body->results;
    }
    
    function getList1($where="",$limit=null,$skip=null,$sort=null){
        $result= $this->rest->getList($this->_classname,$where,$limit,$skip,$sort);
        return $result->body->results;
    }
    
    function count($where=''){
        //$where='{"name":"曹梦龙"}'
        $result= $this->rest->count($this->_classname,$where);
        return $result->body->count;
    }
   
}
