<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Org\Util;

class Sync{
    /**
     * 上推
     */
    public function push($className, $isSync=true){
        //需要同步数据数量
        $count = D($className)->where("syncStatus<>0")->count();
        //分批执行
        $size = 10;
        $page = (int)($count/$size); 
        if(($count%$size) > 0)
            $page ++; 
        $ids = array();
        $error = array();
        for ($i=0; $i<$page; $i++){
            $start = $i*$size;
            //查询数据
            $list = D($className)->field()->where("syncStatus<>0")->limit($start,$size)->select();
            //同步leancloud
            $rest = new Rest();
            //批量处理
            $results = $rest->batch(array('className' => $className, 'results' => $list), $isSync);
            if($results->code == 200)
                foreach ($results->body as $result){
                   // dump($result->success);
                    if($result->success){
                        if($result->success->objectId)
                            array_push($ids, $result->success->objectId);
                    }else
                        array_push($error, $result->error);
                }
            else
                continue;
        }
        $where = array();
        //同步更新
        if(count($ids) > 0)
            array_push($where, array('id'=>array('in',$ids)));
        if(((int)$count)-count($ids)-count($error) > 0)
            array_push($where, array('_logic'=>'OR','syncStatus'=>3));
        if(count($where) > 0)
            D($className)->where($where)->setField('syncStatus',0);
        $message = array('count'=>$count,'syncCount'=>count($ids));
        if(count($error) > 0)
            array_merge($message, array('error'=>$error));
        return $message;
    }
    
    /**
     * 下拉
     */
    public function pull($className){
        $cql = urlencode('select count(*) from '.$className.' where syncStatus<>0');
        $rest = new Rest();
        //获取数据总数
        $result = $rest->query('cql='.$cql);
        if($result->body->count){
            $count = $result->body->count;
            $page = (int)($count/10); 
            if(($count%10) > 0)
                $page ++; 
            for ($i=0; $i<$page; $i++){
                $start = $i*10;
                $end = $start + 10;
                //查询数据
                $cql = urlencode('select * from '.$className.' where syncStatus<>0 limit '.$start.','.$end);
                $result = $rest->query('cql='.$cql);
                if($result->body->results){
                    $data = array();
                    $results = $result->body->results;
                    foreach ($results as $o){
                        $params=array('objectId'=> $o->objectId,'syncStatus'=>2);
                        $syncStatus = $o->syncStatus;
                        $o->syncStatus=0;
                        if($syncStatus == 1)
                            D($className)->add((array)$o);
                        else{
                            if($syncStatus == 3){
                                $o->status=-1;
                                $params['syncStatus'] = 3;
                            }
                            dump($o);
                            D($className)->save((array)$o);
                        }
                        array_push($data, $params);
                    }
                    dump($data);
                    if(count($data) > 0)
                        $results = $rest->batch(array('className' => $className, 'results' => $data), true);
                }
            }
        }
        return array('code'=>0);
    }
}
