<?php
// +----------------------------------------------------------------------
// | ADM Software [ 我宁可 为做过的事而后悔，也不想 因为没尝试而后悔。 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------
namespace Org\Util;

class AVObject {

    private $_classname;
    private $_dataArray = array();
    private $rest;
    public $objectId;
    public $createdAt;
    public $updatedAt;

    /*
     * 构造函数
     */
    function __construct($classname = '') { //初始化对象，将初始化值放在括号内
        $this->_classname = $classname;
        $this->rest = new Rest();
    }

    
    public function set($name, $value) {
        $this->_dataArray[$name] = $value;
        
    } 

    public function get($name) {
        return $this->_dataArray[$name];
    }
    
    public function getArray(){
        $dataArray= $this->_dataArray;
        $dataArray['objectId']=$this->objectId;
        $dataArray['createdAt']=$this->createdAt;
        $dataArray['updatedAt']=$this->updatedAt;
        return $dataArray;
    }

    public function save() {
        //新增加
        if (is_null($this->objectId)) {
            $json = json_encode($this->_dataArray);
            $result = $this->rest->add($this->_classname, $json);
            $this->objectId = $result->body->objectId;
            $this->createdAt= $result->body->createdAt;
            return (array) $result->body;
        }
        //修改
        if (!is_null($this->objectId)) {
            $json = json_encode($this->_dataArray);
            $result = $this->rest->put($this->_classname, $this->objectId, $json);
            $this->objectId = $result->body->objectId;
            $this->updatedAt= $result->body->updatedAt;
            
            return (array) $result->body;
        }
    }
    
    //从云端删除对象
    public function destroy(){
        $result = $this->rest->delete($this->_classname, $this->objectId);
        return (array) $result->body;
    }
    
    public function _QuerySet($body_rwa){
        $this->objectId=$body_rwa['objectId'];
        $this->createdAt=$body_rwa['createdAt'];
        $this->updatedAt=$body_rwa['updatedAt'];
        unset($body_rwa['objectId']);
        unset($body_rwa['createdAt']);
        unset($body_rwa['updatedAt']);
        $this->_dataArray=$body_rwa;
        return $this;
    }

}
