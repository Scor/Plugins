<?php
// +----------------------------------------------------------------------
// | ADM Software [ 我宁可 为做过的事而后悔，也不想 因为没尝试而后悔。 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 扬州安蝶梦电子商务有限公司 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 曹梦龙 <138888611@qq.com> <http://www.andiemeng.com>
// +----------------------------------------------------------------------
namespace Org\Util;

require_once 'httpful/bootstrap.php';


class Rest {
    
    //批量处理
    private $_batch = "batch";
    //查询
    private $_Query = "cloudQuery";
    private $_uriroot = "https://leancloud.cn/1.1/";
    
    //新测试环境  GUORAN -  ///E-Fresh-App-Test///
//    private $_aid = "64yipwcn159l9ehdf3j3x3xz1f1ly9r89x9gf7i2d1282dvz";
//    private $_akey = "w3xp0oibbzhweamm76ogo6r5dyzt82tczw7lssfvyj6hlarv";
//    private $_masterkey = "zlf6rkt286shvykgo08dht3938tg3ln2idvfcu7t61h1ngfq";
    
    //新测试环境  GUORAN -  ///E-Fresh-App-Sandbox///
//    private $_aid = "32co2g21ef1quhvwjl9ok7u4upqvha6d8kv0vas2rstiqbly";
//    private $_akey = "n0xpioh693t6fa3l1kjyu32lbubrr0kj6tsupothw8xl21c9";
//    private $_masterkey = "qh2pmum52zlctzvc9o04aitepvbt75t35snpwkl19398gye7";
    
    //正式环境  GUORAN -  ///E-Fresh-App///
    private $_aid = "8c4dst8wuiimm9nxj8puwrqrhmkxegkl2ittuy6a2jkmx1pl";
    private $_akey = "6lqn0keuf89uzoj8px4t47u9e39eimx7jo7ad1qy9pd125nm";
    private $_masterkey = "lgdpvs1cjc2nab37rlkv6i34hfi6q6r0n87ut5ootaebdndp";
    
    //批量处理
    function batch($params, $isSync){
        $uri = $this->_uriroot . $this->_batch;
        return $this->post($uri ,$this->batchParams($params, $isSync));
    }
    
    //筛选查询
    function query($param){
        $uri = $this->_uriroot . $this->_Query . "?" . $param;
        $response = \Httpful\Request::get($uri)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->send();
        return $response;
    }
    
    function post($uri, $body){
        //dump($body);
        $response = \Httpful\Request::post($uri)
                ->body($body)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->sendsJson()
                ->send();
        return $response;
    }
    
    /**
     * {
     *  'className':'GameScore',
     *  'results':[
     *      {},{},{}...       
     *  ]
     * }
     * 
     * syncStatus 0:同步过后; 1:新增; 2:更新; 3:删除;
     */
    function batchParams($params,$isSync){
        $_className = $params['className'];
        $_requests = array();
        foreach ($params['results'] as $param) {
            if($param['syncStatus'] == 1){
                $param['syncStatus'] = 0;
                if(!$isSync)
                    unset($param['syncStatus']);
                $_obj = array(
                    "method" => "POST",
                    "path" => "/1.1/classes/" . $_className,
                    "body" => $param
                );
                array_push($_requests, $_obj);
            }else if($param['syncStatus'] == 2){
                $param['syncStatus'] = 0;
                if(!$isSync)
                    unset($param['syncStatus']);
                $_obj = array(
                    "method" => "PUT",
                    "path" => "/1.1/classes/" . $_className . "/" . $param['objectId'],
                    "body" => $param
                );
                array_push($_requests, $_obj);
            }else if($param['syncStatus'] == 3){
                $_obj = array(
                    "method" => "DELETE",
                    "path" => "/1.1/classes/" . $_className . "/" . $param['objectId']
                );
                array_push($_requests, $_obj);
            }
        }
        
        return array('requests'=>$_requests);
    }
    
    function hello() {
        $uri = $this->_uriroot . "classes/test/55542ba5e4b032516109abfe";
        $response = \Httpful\Request::get($uri)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->send();
        echo 'hello word';
    }

    //获取数据单个对象
    function get($classname = "", $objectId = "") {
        $uri = $this->_uriroot . "classes/" . $classname . "/" . $objectId;
        $response = \Httpful\Request::get($uri)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->send();
        return $response;
    }
    
    //根据条件获取数据单个对象
    function getFind($classname = "", $where = "") {
        $uri = $this->_uriroot . "classes/" . $classname . "?" . "where=".json_encode($where); 
        $response = \Httpful\Request::get($uri)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->send();
        return $response;
    }

    //获取数据多个对象
    //function getList($classname = '',$where="",$limit=null,$skip=null) {
    function getList($classname = '',$where="",$limit=null,$skip=null,$sort=null) {
        $uri = $this->_uriroot . "classes/" . $classname.'?';
        if($where){
            $urlencodewhere=  urlencode($where);
            $uri=$uri.'&where=' . $urlencodewhere;
        }
        if($limit){
            $uri=$uri.'&limit=' .$limit;
        }
         if($skip){
            $uri=$uri.'&skip=' .$skip;
        }
        if($sort){
            $uri=$uri.'&order=' .$sort;
        }
        $response = \Httpful\Request::get($uri)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->send();
        return $response;
    }
    
    //创建对象
    function add($classname = "", $body = "") {
        $uri = $this->_uriroot . "classes/" . $classname;
        $response = \Httpful\Request::post($uri)
                ->body($body)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->sendsJson()
                ->send();
        return $response;
    }

    //修改对象
    function put($classname = "", $objectId = "", $body = "") {
        $uri = $this->_uriroot . "classes/" . $classname . "/" . $objectId;
        $response = \Httpful\Request::put($uri)
                ->body($body)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                    'X-AVOSCloud-Master-Key' => $this->_masterkey, // in the form of an assoc array
                ))
                ->sendsJson()
                ->send();
        return $response;
    }

    //删除对象
    function delete($classname = "", $objectId = "") {
        $uri = $this->_uriroot . "classes/" . $classname . "/" . $objectId;
        $response = \Httpful\Request::delete($uri)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->send();
        return $response;
    }

    //统计对象数量
    function count($classname = "", $where = "") {
        if ($where) {
            
            $urlencodewhere = urlencode($where);
            $uri = $this->_uriroot . "classes/" . $classname . '?count=1&limit=0&where=' . $urlencodewhere;
        } else {
            $uri = $this->_uriroot . "classes/" . $classname. '?count=1&limit=0';
        }
         
        $response = \Httpful\Request::get($uri)
                //->body($json)
                ->addHeaders(array(
                    'X-AVOSCloud-Application-Id' => $this->_aid, // Or add multiple headers at once
                    'X-AVOSCloud-Application-Key' => $this->_akey, // in the form of an assoc array
                ))
                ->send();
        return $response;
    }

}
