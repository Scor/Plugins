<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Org\Util;

class Juhe {

    private $appkey = '010d9a1b5edb7267c7653aec40bf78f8'; #通过聚合申请到数据的appkey

    function sendSMS($mobile,$tpl_id,$tpl_value) {
        header('content-type:text/html;charset=utf-8');
        $url = 'http://v.juhe.cn/sms/send'; #请求的数据接口URl
        $value=urlencode($tpl_value);
        $params = 'mobile='.$mobile.'&tpl_id='.$tpl_id.'&tpl_value='.$value.'&key=' . $this->appkey;
        //dump($params);
        $content = $this->juhecurl($url, $params, 0);
        //dump($content);
    }

    /*
     * **请求接口，返回JSON数据
     * **@url:接口地址
     * **@params:传递的参数
     * **@ispost:是否以POST提交，默认GET
     */

    function juhecurl($url, $params = false, $ispost = 0) {
        $httpInfo = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($ispost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            if ($params) {
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
                //dump($ch, CURLOPT_URL, $url . '?' . $params);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }
        $response = curl_exec($ch);
        if ($response === FALSE) {
            #echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
        curl_close($ch);
        return $response;
    }

}
